package com.technical;

import static org.assertj.core.api.Assertions.assertThat;

import com.technical.mapper.TransactionCustomMapper;
import com.technical.model.TransactionDto;
import com.technical.model.openbankdto.Details;
import com.technical.model.openbankdto.Holder_;
import com.technical.model.openbankdto.Metadata;
import com.technical.model.openbankdto.OtherAccount;
import com.technical.model.openbankdto.ThisAccount;
import com.technical.model.openbankdto.Transaction;
import com.technical.model.openbankdto.Value;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Test;

public class TransactionCustomMapperTest {

  @Test
  public void shouldMapTransactionsResponseToTransactionDto(){
    List<Transaction> transactions =populateTransactions();

    List<TransactionDto> dto=  TransactionCustomMapper.toTransactionDtoList(populateTransactions());

    assertThat(dto.get(0).getId()).isEqualTo(UUID.fromString("58aeed54-7042-456d-af86-f517bff5b7af"));
    assertThat(dto.get(0).getTransactionType()).isEqualTo("SEPA");
    assertThat(dto.get(0).getTransactionAmount()).isEqualTo(40);
    assertThat(dto.get(0).getCounterpartyAccount()).isEqualTo("number");
    assertThat(dto.get(0).getCounterpartyName()).isEqualTo("NAME");
    assertThat(dto.get(0).getCounterPartyLogoPath()).isEqualTo("IMAGE_URL");
    assertThat(dto.get(0).getInstructedAmount()).isEqualTo(40.0);
    assertThat(dto.get(0).getInstructedCurrency()).isEqualTo("GBP");
  }

  private List<Transaction> populateTransactions() {
    List<Transaction> transactions = new ArrayList<>();
    Transaction transaction = new Transaction();
    transaction.setId(UUID.fromString("58aeed54-7042-456d-af86-f517bff5b7af"));
    ThisAccount thisAccount = new ThisAccount();
    thisAccount.setId("ID");
    transaction.setThisAccount(thisAccount);

    OtherAccount otherAccount = new OtherAccount();
    otherAccount.setNumber("number");
    Holder_ holder = new Holder_();
    holder.setName("NAME");
    otherAccount.setHolder(holder);
    Metadata metadata = new Metadata();
    metadata.setImageURL("IMAGE_URL");
    otherAccount.setMetadata(metadata);
    transaction.setOtherAccount(otherAccount);

    Details details = new Details();
    details.setType("SEPA");
    details.setDescription("This is a SEPA Transaction Request");

    Value value = new Value();
    value.setAmount(40.0);
    value.setCurrency("GBP");
    details.setValue(value);
    transaction.setDetails(details);

    transactions.add(transaction);
    return transactions;
  }
}
