package com.technical;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import com.technical.mapper.TransactionCustomMapper;
import com.technical.model.TransactionDto;
import com.technical.model.openbankdto.Details;
import com.technical.model.openbankdto.Holder_;
import com.technical.model.openbankdto.Metadata;
import com.technical.model.openbankdto.OtherAccount;
import com.technical.model.openbankdto.ThisAccount;
import com.technical.model.openbankdto.Transaction;
import com.technical.model.openbankdto.Value;
import com.technical.service.TransactionService;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
public class TransactionServiceTest {

  @MockBean
  private TransactionService transactionService;

  @Test
  public void ShouldReturnMappedTransactions() {
    when(transactionService.getTransactions())
        .thenReturn(TransactionCustomMapper.toTransactionDtoList(populateTransactions()));

    assertThat(transactionService.getTransactions()).isNotEmpty();
  }

  @Test
  public void ShouldReturnMappedTransactionsFilterByType() {
    String type="SEPA";
    when(transactionService.getTransactionsByType(type))
        .thenReturn(TransactionCustomMapper.toTransactionDtoList(populateTransactions()));
    List<TransactionDto> trans = transactionService.getTransactionsByType(type);
    assertThat(trans).isNotEmpty();
    assertTrue(trans.stream()
        .allMatch(transactionDto -> transactionDto.getTransactionType().equals(type)));
  }

  @Test
  public void ShouldReturnSumOfTransactionAmountByType() {
    String type="SEPA";
    when(transactionService.getTransactionsByType(type))
        .thenReturn(TransactionCustomMapper.toTransactionDtoList(populateTransactions()));

    assertThat(transactionService.getTotalAmountForTransactionType(type)).isEqualTo(0.0);
  }

  private List<Transaction> populateTransactions() {
    List<Transaction> transactions = new ArrayList<>();
    Transaction transaction = new Transaction();
    ThisAccount thisAccount = new ThisAccount();
    thisAccount.setId("ID");
    transaction.setThisAccount(thisAccount);

    OtherAccount otherAccount = new OtherAccount();
    otherAccount.setNumber("number");
    Holder_ holder = new Holder_();
    holder.setName("NAME");
    otherAccount.setHolder(holder);
    Metadata metadata = new Metadata();
    metadata.setImageURL("IMAGE_URL");
    otherAccount.setMetadata(metadata);
    transaction.setOtherAccount(otherAccount);

    Details details = new Details();
    details.setType("SEPA");
    details.setDescription("This is a SEPA Transaction Request");

    Value value = new Value();
    value.setAmount(40.0);
    value.setCurrency("GBP");
    details.setValue(value);
    transaction.setDetails(details);

    transactions.add(transaction);
    return transactions;
  }
}
