package com.technical.configuration;

import java.util.Collections;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriTemplateHandler;

@Configuration
public class RestClient {

  @Bean
  public RestTemplate restTemplate() {
    RestTemplate restClient = new RestTemplate(
        new BufferingClientHttpRequestFactory(new SimpleClientHttpRequestFactory()));
    DefaultUriTemplateHandler defaultUriTemplateHandler = new DefaultUriTemplateHandler();
    defaultUriTemplateHandler.setBaseUrl("https://apisandbox.openbankproject.com/obp/v1.2.1/");
    restClient.setUriTemplateHandler(defaultUriTemplateHandler);
    restClient.setInterceptors(Collections.singletonList((request, body, execution) -> execution.execute(request, body)));
    return restClient;
  }
}
