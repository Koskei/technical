
package com.technical.model.openbankdto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Bank_ {

  @JsonProperty("national_identifier")
  private Object nationalIdentifier;
  private String name;

  public Object getNationalIdentifier() {
    return nationalIdentifier;
  }

  public void setNationalIdentifier(Object nationalIdentifier) {
    this.nationalIdentifier = nationalIdentifier;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
