
package com.technical.model.openbankdto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OtherAccount {

    private String id;
    private Holder_ holder;
    private String number;
    private String kind;
    @JsonProperty("IBAN")
    private String iBAN;
    @JsonProperty("swift_bic")
    private Object swiftBic;
    private Bank_ bank;
    private Metadata metadata;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Holder_ getHolder() {
        return holder;
    }

    public void setHolder(Holder_ holder) {
        this.holder = holder;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getiBAN() {
        return iBAN;
    }

    public void setiBAN(String iBAN) {
        this.iBAN = iBAN;
    }

    public Object getSwiftBic() {
        return swiftBic;
    }

    public void setSwiftBic(Object swiftBic) {
        this.swiftBic = swiftBic;
    }

    public Bank_ getBank() {
        return bank;
    }

    public void setBank(Bank_ bank) {
        this.bank = bank;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }
}
