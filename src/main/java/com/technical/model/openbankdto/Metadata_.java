
package com.technical.model.openbankdto;

import java.util.ArrayList;
import java.util.List;


public class Metadata_ {

    private Object narrative;
    private List<Object> comments = new ArrayList<>();
    private List<Object> tags = new ArrayList<>();
    private List<Object> images = new ArrayList<>();
    private Object where;

    public Object getNarrative() {
        return narrative;
    }

    public void setNarrative(Object narrative) {
        this.narrative = narrative;
    }

    public List<Object> getComments() {
        return comments;
    }

    public void setComments(List<Object> comments) {
        this.comments = comments;
    }

    public List<Object> getTags() {
        return tags;
    }

    public void setTags(List<Object> tags) {
        this.tags = tags;
    }

    public List<Object> getImages() {
        return images;
    }

    public void setImages(List<Object> images) {
        this.images = images;
    }

    public Object getWhere() {
        return where;
    }

    public void setWhere(Object where) {
        this.where = where;
    }
}
