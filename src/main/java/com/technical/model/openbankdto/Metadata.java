
package com.technical.model.openbankdto;

import com.fasterxml.jackson.annotation.JsonProperty;


public class Metadata {

    @JsonProperty("public_alias")
    private Object publicAlias;
    @JsonProperty("private_alias")
    private Object privateAlias;
    @JsonProperty("more_info")
    private Object moreInfo;
    @JsonProperty("URL")
    private Object uRL;
    @JsonProperty("image_URL")
    private String imageURL;
    @JsonProperty("open_corporates_URL")
    private Object openCorporatesURL;
    @JsonProperty("corporate_location")
    private Object corporateLocation;
    @JsonProperty("physical_location")
    private Object physicalLocation;

    public Object getPublicAlias() {
        return publicAlias;
    }

    public void setPublicAlias(Object publicAlias) {
        this.publicAlias = publicAlias;
    }

    public Object getPrivateAlias() {
        return privateAlias;
    }

    public void setPrivateAlias(Object privateAlias) {
        this.privateAlias = privateAlias;
    }

    public Object getMoreInfo() {
        return moreInfo;
    }

    public void setMoreInfo(Object moreInfo) {
        this.moreInfo = moreInfo;
    }

    public Object getuRL() {
        return uRL;
    }

    public void setuRL(Object uRL) {
        this.uRL = uRL;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public Object getOpenCorporatesURL() {
        return openCorporatesURL;
    }

    public void setOpenCorporatesURL(Object openCorporatesURL) {
        this.openCorporatesURL = openCorporatesURL;
    }

    public Object getCorporateLocation() {
        return corporateLocation;
    }

    public void setCorporateLocation(Object corporateLocation) {
        this.corporateLocation = corporateLocation;
    }

    public Object getPhysicalLocation() {
        return physicalLocation;
    }

    public void setPhysicalLocation(Object physicalLocation) {
        this.physicalLocation = physicalLocation;
    }
}
