
package com.technical.model.openbankdto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.UUID;


public class Transaction {

    private UUID id;
    @JsonProperty("this_account")
    private ThisAccount thisAccount;
    @JsonProperty("other_account")
    private OtherAccount otherAccount;
    private Details details;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public ThisAccount getThisAccount() {
        return thisAccount;
    }

    public void setThisAccount(ThisAccount thisAccount) {
        this.thisAccount = thisAccount;
    }

    public OtherAccount getOtherAccount() {
        return otherAccount;
    }

    public void setOtherAccount(OtherAccount otherAccount) {
        this.otherAccount = otherAccount;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

}
