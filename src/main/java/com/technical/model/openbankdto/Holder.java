
package com.technical.model.openbankdto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Holder {

    private String name;
    @JsonProperty("is_alias")
    private Boolean isAlias;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getAlias() {
        return isAlias;
    }

    public void setAlias(Boolean alias) {
        isAlias = alias;
    }
}
