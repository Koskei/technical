package com.technical.service.impl;

import static com.technical.mapper.TransactionCustomMapper.toTransactionDtoList;

import com.technical.model.TransactionDto;
import com.technical.model.openbankdto.Transactions;
import com.technical.service.TransactionService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class TransactionServiceImpl implements TransactionService {

  private final RestTemplate restTemplate;

  public TransactionServiceImpl(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  @Override
  public List<TransactionDto> getTransactions() {
    return clientRequestTransactions();
  }

  @Override
  public List<TransactionDto> getTransactionsByType(String type) {
    return clientRequestTransactions()
        .stream().filter(t -> type.equals(t.getTransactionType())).collect(Collectors.toList());
  }

  @Override
  public double getTotalAmountForTransactionType(String type) {
    List<TransactionDto> transactions = clientRequestTransactions()
        .stream().filter(t -> type.equals(t.getTransactionType())).collect(Collectors.toList());
    return transactions.stream().mapToDouble(TransactionDto::getTransactionAmount).sum();
  }

  public List<TransactionDto> clientRequestTransactions() {
    Transactions response
        = restTemplate.getForObject("banks/rbs/accounts/savings-kids-john/public/transactions", Transactions.class);
    return toTransactionDtoList(response.getTransactions());
  }
}
