package com.technical.service;

import com.technical.model.TransactionDto;
import java.util.List;
import org.springframework.beans.factory.annotation.Qualifier;

public interface TransactionService {

  List<TransactionDto> getTransactions();

  List<TransactionDto> getTransactionsByType(String type);

  double getTotalAmountForTransactionType(String type);
}
