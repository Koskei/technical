package com.technical.controller;

import com.technical.model.TransactionDto;
import com.technical.service.TransactionService;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/transactions")
public class TransactionController {

  private final TransactionService transactionService;

  public TransactionController(TransactionService transactionService) {
    this.transactionService = transactionService;
  }

  @GetMapping("/list")
  public List<TransactionDto> getTransactions(){
    return transactionService.getTransactions();
  }

  @GetMapping("/list/{type}")
  public  List<TransactionDto>  getTransactionsByType(@PathVariable String type){
    return transactionService.getTransactionsByType(type);
  }

  @GetMapping("/amount/{type}")
  public double getTotalAmountForTransactionType(@PathVariable String type){
    return transactionService.getTotalAmountForTransactionType(type);
  }
}
