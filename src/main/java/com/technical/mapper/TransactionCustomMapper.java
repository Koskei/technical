package com.technical.mapper;

import com.technical.model.TransactionDto;
import com.technical.model.openbankdto.Transaction;
import java.util.List;
import java.util.stream.Collectors;

public class TransactionCustomMapper {

  public static List<TransactionDto> toTransactionDtoList(List<Transaction> transactions) {
   return transactions.stream().map(TransactionCustomMapper::toTransactionDto).collect(Collectors.toList());
  }

  private static TransactionDto toTransactionDto(Transaction transaction) {
    TransactionDto dto = new TransactionDto();
    dto.setId(transaction.getId());
    dto.setAccountId(transaction.getId());
    dto.setCounterpartyAccount(transaction.getOtherAccount().getNumber());
    dto.setCounterpartyName(transaction.getOtherAccount().getHolder().getName());
    dto.setCounterPartyLogoPath(transaction.getOtherAccount().getMetadata().getImageURL());
    dto.setInstructedAmount(transaction.getDetails().getValue().getAmount());
    dto.setInstructedCurrency(transaction.getDetails().getValue().getCurrency());
    dto.setTransactionAmount(transaction.getDetails().getValue().getAmount());
    dto.setTransactionCurrency(transaction.getDetails().getValue().getCurrency());
    dto.setTransactionType(transaction.getDetails().getType());
    dto.setDescription(transaction.getDetails().getDescription());
    return dto;
  }
}
